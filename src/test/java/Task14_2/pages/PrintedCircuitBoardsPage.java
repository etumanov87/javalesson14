package Task14_2.pages;

import com.codeborne.selenide.SelenideElement;

import java.time.Duration;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class PrintedCircuitBoardsPage {
    SelenideElement fieldLength = $x("//input[@placeholder='длина']");
    SelenideElement fieldWidth = $x("//input[@placeholder='ширина']");
    SelenideElement fieldNumberOfPieces = $x("//input[@name='count']");
    SelenideElement buttonCalculate = $x("//button[@id='calculate']");
    SelenideElement checkVisibleTextWithPrice = $x("//span[@id='total-price']");
    SelenideElement checkVisibleBlockWithRedWarningIcon = $x("//div[@class='alert alert-danger']");
    SelenideElement checkVisibleTextIncorrectData = $x("//div[text()='Ширина болжна быть от 5 до 475 мм']");

    public void setFieldLength(int valueLength) {
        fieldLength.setValue(String.valueOf(valueLength));
    }

    public void setFieldWidth(int valueWidth) {
        fieldWidth.setValue(String.valueOf(valueWidth));
    }

    public void setFieldNumberOfPieces(int valueNumberOfPieces) {
        fieldNumberOfPieces.setValue(String.valueOf(valueNumberOfPieces));
    }

    public void buttonCalculate() {
        buttonCalculate.click();
    }

    public void checkVisibleTextWithPrice(int valueDurationTimeout) {
        checkVisibleTextWithPrice.shouldBe(visible, Duration.ofSeconds(valueDurationTimeout));
    }

    public void checkVisibleBlockWithRedWarningIcon() {
        checkVisibleBlockWithRedWarningIcon.shouldBe(visible);

    }

    public void checkVisibleTextIncorrectData() {
        checkVisibleTextIncorrectData.shouldBe(visible);
    }

}
