package Task14_2.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Configuration.pageLoadTimeout;
import static com.codeborne.selenide.Selenide.$x;

public class HomePage {

    SelenideElement itemUrgentPrintedCircuitBoards = $x("//ul[contains(@class,'main-menu')]//a[contains(text(),'Срочные печатные')]");

    public void clickItemUrgentPrintedCircuitBoards() {
        itemUrgentPrintedCircuitBoards.click();
    }

    public void open(int durationTimeout) {
        pageLoadTimeout = durationTimeout;
        Selenide.open("https://www.rezonit.ru/");
    }
}
