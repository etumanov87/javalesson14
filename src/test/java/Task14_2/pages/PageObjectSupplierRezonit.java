package Task14_2.pages;

public interface PageObjectSupplierRezonit {
    default HomePage homePage() {
        return new HomePage();
    }

    default PrintedCircuitBoardsPage printedCircuitBoardsPage() {
        return new PrintedCircuitBoardsPage();
    }
}
