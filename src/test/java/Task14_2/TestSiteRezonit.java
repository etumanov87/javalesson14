package Task14_2;


import Task14_2.pages.PageObjectSupplierRezonit;
import org.testng.annotations.Test;

public class TestSiteRezonit implements PageObjectSupplierRezonit {

    @Test
    public void testPositiveScenarioWebsite() {
        homePage().open(50000);
        homePage().clickItemUrgentPrintedCircuitBoards();
        printedCircuitBoardsPage().setFieldLength(10);
        printedCircuitBoardsPage().setFieldWidth(20);
        printedCircuitBoardsPage().setFieldNumberOfPieces(20);
        printedCircuitBoardsPage().buttonCalculate();
        printedCircuitBoardsPage().checkVisibleTextWithPrice(20);
    }


    @Test
    public void testNegativeScenarioWebsite() {
        homePage().open(50000);
        homePage().clickItemUrgentPrintedCircuitBoards();
        printedCircuitBoardsPage().setFieldLength(10);
        printedCircuitBoardsPage().setFieldWidth(-999);
        printedCircuitBoardsPage().checkVisibleBlockWithRedWarningIcon();
        printedCircuitBoardsPage().checkVisibleTextIncorrectData();
    }

}
