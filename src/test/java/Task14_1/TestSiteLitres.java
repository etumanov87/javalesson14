package Task14_1;


import Task14_1.pages.PageObjectSupplierLitres;
import org.testng.annotations.Test;


public class TestSiteLitres implements PageObjectSupplierLitres {

    @Test
    public void testCheckSiteLitres() {

        homePage().open();
        homePage().setSearchBar();
        resultSearchPage().selectFirstProductSearchResults();
        productPage().checkVisiblePriceSubscription();
        productPage().checkVisibleButtonAddToCart();
        productPage().checkVisiblePriceSubscription();

    }
}


