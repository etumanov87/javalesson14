package Task14_1.pages;

public interface PageObjectSupplierLitres {
    default HomePage homePage() {
        return new HomePage();
    }

    default ResultSearchPage resultSearchPage(){
        return new ResultSearchPage();
    }

    default ProductPage productPage(){
        return new ProductPage();
    }
}
