package Task14_1.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class ProductPage {
    SelenideElement priceSubscription = $x("//div[@data-test-id='book-sale-block__abonement--wrapper']");
    SelenideElement priceForDownload = $x("//div[@data-test-id='book-sale-block__PPD--wrapper']");
    SelenideElement buttonAddToCart = $x("//button[@data-test-id='book__addToCartButton--desktop']");

    public void checkVisiblePriceSubscription() {
        priceSubscription.shouldBe(visible);
    }

    public void checkVisiblePriceForDownload() {
        priceForDownload.shouldBe(visible);
    }

    public void checkVisibleButtonAddToCart() {
        buttonAddToCart.shouldBe(visible);
    }

}
