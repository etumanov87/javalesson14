package Task14_1.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;

public class HomePage {
    SelenideElement searchBar = $x("//input[@data-test-id='header__search-input--desktop']");
    SelenideElement buttonSearchBar = $x("//button[@data-test-id='header__search-button--desktop']");

    public void setSearchBar(){
        searchBar.setValue("грокаем алгоритмы");
        buttonSearchBar.click();
    }

    public void open() {
        Selenide.open("https://www.litres.ru/");
    }
}
