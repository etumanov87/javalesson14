package Task14_1.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class ResultSearchPage {
    SelenideElement firstProductSearchResults = $x("//div[@data-test-id='art__cover--desktop']");

    public void selectFirstProductSearchResults() {
        firstProductSearchResults.click();
    }
}
